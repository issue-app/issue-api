import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { CustomError } from '../error/types';
import { createGenericResponse } from '../responses/response';

function isCustomError(obj: unknown): obj is CustomError {
  return typeof (<CustomError>obj).errorCode === 'number';
}

function isHttpException(obj: unknown): obj is HttpException {
  return typeof (<HttpException>obj).getResponse?.() === 'object';
}

interface HttpError {
  statusCode: number;
  message: string;
  error: string;
}

@Catch(Error)
export class HttpExceptionErrorFilter implements ExceptionFilter<Error> {
  catch(error: Error, host: ArgumentsHost): void {
    if (isCustomError(error)) {
      this.handleCustomError(error, host);
      return;
    }

    if (isHttpException(error)) {
      this.handleHttpException(error, host);
      return;
    }

    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    response.status(500).json(
      createGenericResponse(
        null,
        [
          {
            message: error.message,
            code: 500,
            name: 'SYSTEM_ERROR',
            payload: null,
          },
        ],
        500,
      ),
    );
  }

  handleCustomError(error: CustomError, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    response.status(error.httpCode).json(
      createGenericResponse(
        null,
        [
          {
            message: error.message,
            code: error.errorCode,
            name: error.name,
            payload: error.payload,
          },
        ],
        error.httpCode,
      ),
    );
  }

  handleHttpException(error: HttpException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const { statusCode = 500, message } = error.getResponse() as HttpError;
    response.status(statusCode).json(
      createGenericResponse(
        null,
        [
          {
            message: message,
            code: statusCode,
            name: 'HTTP_EXCEPTION',
            payload: null,
          },
        ],
        statusCode,
      ),
    );
  }
}
