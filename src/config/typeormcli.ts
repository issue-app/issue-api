import { DataSource } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import * as dotenv from 'dotenv';
const dotenvPaths = ['.env', '.env.dev'];
export default new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5444,
  username: 'postgres',
  password: 'postgres',
  database: 'development',
  synchronize: process.env.DB_SYNC_ENABLED === 'true',
  migrationsRun: process.env.doDB_MIGRATIONS_ENABLED === 'true',
  logging: false,
  entities: ['dist/**/*.entity{ .ts,.js}'],
  migrations: ['dist/migrations/*{.ts,.js}'],
  uuidExtension: 'pgcrypto',
  schema: 'public',
  migrationsTableName: 'migrations',
  namingStrategy: new SnakeNamingStrategy(),
});
