import { Injectable } from '@nestjs/common';
import {
  CreateIssueRequest,
  GetIssuesResponse,
  PaginatedIssues,
  PaginationOptions,
  UpdateIssueRequest,
} from './issue.types';
import { User } from '../auth/entities/user.entity';
import { Issue, IssueStatusType } from './entity/issue.entity';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { throwError } from '../error/error';
import { CustomErrors } from '../error/customError';

@Injectable()
export class IssueService {
  constructor(
    private jwtService: JwtService,
    @InjectRepository(Issue)
    private readonly issueRepository: Repository<Issue>,
  ) {}
  async createIssue(
    { title, description, status }: CreateIssueRequest,
    user: User,
  ): Promise<Issue> {
    const issue: Issue = new Issue();
    issue.title = title;
    issue.description = description;
    issue.user = user;
    issue.status = status;

    return this.issueRepository.save(issue);
  }

  async findAndCountAllIssues(
    paginationOptions: PaginationOptions,
    user: User,
  ): Promise<PaginatedIssues> {
    const { page, limit }: PaginationOptions = paginationOptions;
    const skip: number = (page - 1) * limit;

    const [issues, total] = await this.issueRepository
      .createQueryBuilder('issue')
      .leftJoinAndSelect('issue.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .andWhere('issue.status != :status', { status: IssueStatusType.DELETED })
      .skip(skip)
      .take(limit)
      .getManyAndCount();

    const totalItems: number = total;
    const itemsPerPage = Number(limit);
    const totalPages: number = Math.ceil(total / limit);
    const currentPage = Number(page);

    return {
      issues,
      pagination: {
        totalItems,
        itemsPerPage,
        totalPages,
        currentPage,
      },
    };
  }

  async getIssueById(id: string, user: User): Promise<Issue> {
    const issue: Issue = await this.issueRepository
      .createQueryBuilder('issue')
      .leftJoinAndSelect('issue.user', 'user')
      .where('issue.id = :id AND issue.status != :status ', {
        id,
        status: IssueStatusType.DELETED,
      })
      .getOne();
    if (!issue) {
      throwError(CustomErrors.ISSUE_NOT_FOUND);
    }

    const isAuthorized = issue.user.id === user.id;
    if (!isAuthorized) throwError(CustomErrors.UNAUTHORIZED_ACCESS);
    return issue;
  }

  async updateIssue(
    id: string,
    user: User,
    updateIssueRequest: UpdateIssueRequest,
  ): Promise<Issue> {
    const issue: Issue = await this.getIssueById(id, user);

    const updatedIssue = { ...issue, ...updateIssueRequest };

    return this.issueRepository.save(updatedIssue);
  }

  async deleteIssue(id: string, user: User) {
    const issue: Issue = await this.getIssueById(id, user);

    await this.issueRepository.update(issue.id, {
      status: IssueStatusType.DELETED,
    });

    return { success: true };
  }
}
