import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { IssueController } from './issue.controller';
import { IssueService } from './issue.service';
import { SessionMiddleware } from '../responses/auth.middleware';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constancts';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Issue } from './entity/issue.entity';
import { CommentModule } from '../comment/comment.module';
import { FileModule } from '../file/file.module';

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([Issue]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
    CommentModule,
    FileModule,
  ],
  controllers: [IssueController],
  providers: [IssueService],
})
export class IssueModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(SessionMiddleware).forRoutes(IssueController);
  }
}
