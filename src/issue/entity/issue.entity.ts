import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { User } from '../../auth/entities/user.entity';
import { Comment } from '../../comment/entity/comment.entity';
import { File } from '../../file/entity/file.entity';

export enum IssueStatusType {
  PENDING = 'pending',
  COMPLETED = 'completed',
  DELETED = 'deleted',
}

@Entity({ name: 'issue' })
export class Issue {
  @ApiProperty({
    format: 'uuid',
    example: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
  })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({ example: 'Filtering trx not working as expected' })
  title: string;

  @ApiProperty({ example: 'john.doe' })
  @Column()
  description: string;

  @ManyToOne(() => User)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'id',
  })
  user: User;

  @Column({
    length: 20,
  })
  status: IssueStatusType;

  @OneToMany(() => Comment, (comment) => comment.issue)
  comments: Comment[];

  @OneToMany(() => File, (file) => file.issue)
  files: File[];

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
    comment: 'Create time',
  })
  public createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
    comment: 'Last update time',
  })
  public updatedAt: Date;
}
