import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { GenericResponse } from '../responses/response';
import { Issue } from './entity/issue.entity';
import { File } from '../file/entity/file.entity';
import { Comment } from '../comment/entity/comment.entity';

export enum IssueStatusType {
  PENDING = 'pending',
  COMPLETED = 'completed',
}

export class CreateCommentRequest {
  @ApiProperty({ example: 'This is a comment for the issue' })
  @IsNotEmpty()
  @IsString()
  text: string;
}

export class CreateIssueRequest {
  @ApiProperty({ example: 'Filtering trx not working as expected' })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty({
    example:
      'The following issue appears on prod environment in Dashboard and Cards section',
  })
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiProperty({ example: 'pending' })
  @IsEnum(IssueStatusType)
  public readonly status: IssueStatusType;
}

export class UpdateIssueRequest {
  @ApiPropertyOptional({ example: 'Filtering trx not working as expected' })
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  title: string;

  @ApiPropertyOptional({
    example:
      'The following issue appears on prod environment in Dashboard and Cards section',
  })
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  description?: string;

  @ApiPropertyOptional({ example: 'pending' })
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @IsEnum(IssueStatusType)
  public readonly status?: IssueStatusType;
}

export class IssueResponse extends GenericResponse<Issue> {
  @ApiProperty({
    type: Issue,
  })
  data: Issue;
}

export class FileResponse extends GenericResponse<File> {
  @ApiProperty({
    type: File,
  })
  data: File;
}

export class CommentResponse extends GenericResponse<Comment> {
  @ApiProperty({
    type: Comment,
  })
  data: Comment;
}

export class Pagination {
  @ApiProperty()
  totalItems: number;
  @ApiProperty()
  itemsPerPage: number;
  @ApiProperty()
  totalPages: number;
  @ApiProperty()
  currentPage: number;
}

export class PaginatedIssues {
  @ApiProperty({ type: Issue, isArray: true })
  issues: Issue[];
  @ApiProperty()
  pagination: Pagination;
}

export class GetIssuesResponse extends GenericResponse<PaginatedIssues> {
  @ApiProperty({
    type: PaginatedIssues,
  })
  data: PaginatedIssues;
}

export class PaginatedComments {
  @ApiProperty({ type: Comment, isArray: true })
  comments: Comment[];
  @ApiProperty()
  pagination: Pagination;
}

export class GetCommentsResponse extends GenericResponse<PaginatedComments> {
  @ApiProperty({
    type: PaginatedComments,
  })
  data: PaginatedComments;
}

export class PaginatedFiles {
  @ApiProperty({ type: File, isArray: true })
  files: File[];
  @ApiProperty()
  pagination: Pagination;
}

export class GetFilesResponse extends GenericResponse<PaginatedFiles> {
  @ApiProperty({
    type: PaginatedFiles,
  })
  data: PaginatedFiles;
}

export class PaginationOptions {
  @ApiProperty({ example: 1, description: 'Page number', required: false })
  @IsNumber()
  @IsPositive()
  readonly page: number = 1;

  @ApiProperty({
    example: 10,
    description: 'Number of items per page',
    required: false,
  })
  @IsNumber()
  @IsPositive()
  readonly limit: number = 10;
}

export class SuccessDeterminationResponseData {
  @ApiProperty()
  success: boolean;
}

export class SuccessDeterminationResponse extends GenericResponse<SuccessDeterminationResponseData> {
  @ApiProperty({
    type: SuccessDeterminationResponseData,
  })
  data: SuccessDeterminationResponseData;
}
