import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  UploadedFile,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { IssueService } from './issue.service';
import {
  createGenericResponse,
  CustomApiApiBadRequestResponse,
  CustomApiUnauthorizedResponse,
} from '../responses/response';
import {
  CreateIssueRequest,
  IssueResponse,
  GetIssuesResponse,
  PaginatedIssues,
  PaginationOptions,
  UpdateIssueRequest,
  SuccessDeterminationResponse,
  SuccessDeterminationResponseData,
  CreateCommentRequest,
  CommentResponse,
  PaginatedComments,
  GetCommentsResponse,
  FileResponse,
  GetFilesResponse,
  PaginatedFiles,
} from './issue.types';
import { ActiveUser } from '../decorators/active-user.decorator';
import { User } from '../auth/entities/user.entity';
import { Issue } from './entity/issue.entity';
import { CommentService } from '../comment/comment.service';
import { Comment } from '../comment/entity/comment.entity';
import { PhotoUpload } from '../decorators/photoUpload.decorator';
import { Express } from 'express';
import { FileService } from '../file/file.service';
import { File } from '../file/entity/file.entity';

@Controller('v1/issues')
@ApiTags('Issue')
@ApiBearerAuth()
export class IssueController {
  constructor(
    private readonly issueService: IssueService,
    private readonly commentService: CommentService,
    private readonly fileService: FileService,
  ) {}

  @Post('')
  @ApiOperation({
    summary: 'Create an issue',
    description: 'Creates new issue.',
  })
  @ApiBody({
    type: CreateIssueRequest,
  })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  @ApiOkResponse({
    description: 'Issue created',
    type: IssueResponse,
  })
  async createIssue(
    @ActiveUser() user: User,
    @Body() createIssueRequest: CreateIssueRequest,
  ): Promise<IssueResponse> {
    const response = await this.issueService.createIssue(
      createIssueRequest,
      user,
    );
    return createGenericResponse(response, null, 201);
  }

  @Get('')
  @ApiOperation({
    summary: 'Get paginated List of issue',
    description: 'Returns users paginated issue list',
  })
  @ApiOkResponse({
    description: 'List of issues returned',
    type: GetIssuesResponse,
  })
  @ApiQuery({ name: 'page', type: Number, required: false })
  @ApiQuery({ name: 'limit', type: Number, required: false })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  async getIssues(
    @ActiveUser() user: User,
    @Query() paginationOptions: PaginationOptions,
  ): Promise<GetIssuesResponse> {
    const response: PaginatedIssues =
      await this.issueService.findAndCountAllIssues(paginationOptions, user);
    return createGenericResponse(response);
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Get issue by id',
    description: 'Returns users issue by ID',
  })
  @ApiOkResponse({
    description: 'Issue created',
    type: IssueResponse,
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  async getIssueById(
    @ActiveUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<IssueResponse> {
    const response: Issue = await this.issueService.getIssueById(id, user);
    return createGenericResponse(response);
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'Updates the issue',
    description: 'Returns updated issue',
  })
  @ApiOkResponse({
    description: 'Issue updated',
    type: IssueResponse,
  })
  @ApiBody({
    type: UpdateIssueRequest,
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  async updateIssue(
    @ActiveUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() updateIssueRequest: UpdateIssueRequest,
  ): Promise<IssueResponse> {
    const response: Issue = await this.issueService.updateIssue(
      id,
      user,
      updateIssueRequest,
    );
    return createGenericResponse(response);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete the issue',
    description: 'Removes the issue from the system',
  })
  @ApiOkResponse({
    description: 'Issue updated',
    type: SuccessDeterminationResponse,
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  async deleteIssue(
    @ActiveUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<SuccessDeterminationResponse> {
    const response: SuccessDeterminationResponseData =
      await this.issueService.deleteIssue(id, user);
    return createGenericResponse(response);
  }

  @Post(':id/comments')
  @ApiOperation({
    summary: 'Create the comment related to the issue',
    description: 'Creates new comment.',
  })
  @ApiBody({
    type: CreateCommentRequest,
  })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  @ApiOkResponse({
    description: 'Create Comment Response',
    type: CommentResponse,
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  async createComment(
    @ActiveUser() user: User,
    @Body() createCommentRequest: CreateCommentRequest,
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<CommentResponse> {
    const issue: Issue = await this.issueService.getIssueById(id, user);

    const response: Comment = await this.commentService.createComment(
      issue,
      createCommentRequest,
    );
    return createGenericResponse(response, null, 201);
  }

  @Get(':id/comments')
  @ApiOperation({
    summary: 'Get the comments related to the issue',
    description: 'Returns the list of comments related to the issue',
  })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  @ApiOkResponse({
    description: 'Paginated list of comments response',
    type: GetCommentsResponse,
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  async getComments(
    @ActiveUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
    @Query() paginationOptions: PaginationOptions,
  ): Promise<GetCommentsResponse> {
    const issue: Issue = await this.issueService.getIssueById(id, user);

    const response: PaginatedComments =
      await this.commentService.findAndCountAllComments(
        paginationOptions,
        issue,
      );
    return createGenericResponse(response);
  }

  @Post(':id/files')
  @ApiOperation({
    summary: 'Uploads a file related to the issue',
    description: 'Uploads a file related to the issue.',
  })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  @ApiOkResponse({
    description: 'Upload file response',
    type: FileResponse,
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  @PhotoUpload()
  async uploadFile(
    @ActiveUser() user: User,
    @UploadedFile() file: Express.Multer.File,
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<FileResponse> {
    const issue: Issue = await this.issueService.getIssueById(id, user);

    const response = await this.fileService.uploadFile(
      file.originalname,
      file.mimetype,
      file.buffer,
      issue,
    );
    return createGenericResponse(response);
  }

  @Get(':id/files')
  @ApiOperation({
    summary: 'Get the files related to the issue',
    description: 'Return paginated list of files related to the issue',
  })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  @ApiOkResponse({
    description: 'Paginated Files response',
    type: GetFilesResponse,
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  async getFiles(
    @ActiveUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
    @Query() paginationOptions: PaginationOptions,
  ): Promise<GetFilesResponse> {
    const issue: Issue = await this.issueService.getIssueById(id, user);

    const response: PaginatedFiles =
      await this.fileService.findAndCountAllFiles(paginationOptions, issue);
    return createGenericResponse(response);
  }

  @Get(':id/files/:fileId')
  @ApiOperation({
    summary: 'Get the files related to the issue',
    description: 'Return paginated list of files related to the issue',
  })
  @CustomApiUnauthorizedResponse()
  @CustomApiApiBadRequestResponse()
  @ApiOkResponse({
    description: 'FileResponse',
    type: FileResponse,
  })
  @ApiParam({ name: 'id', type: String, description: 'Id of the issue' })
  @ApiParam({ name: 'fileId', type: String, description: 'Id of the issue' })
  async getFile(
    @ActiveUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
    @Param('fileId', new ParseUUIDPipe()) fileId: string,
  ): Promise<FileResponse> {
    const response: File = await this.fileService.getFileByIdAndIssue(
      id,
      fileId,
    );
    return createGenericResponse(response);
  }
}
