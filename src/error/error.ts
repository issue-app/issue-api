import { CustomError, ErrorThrowDefinition } from './types';

export const buildError = (
  errorDefinition: ErrorThrowDefinition,
  payload = {},
  messageOverride = null,
): CustomError => {
  return new CustomError(
    errorDefinition?.name,
    messageOverride ?? errorDefinition?.message,
    errorDefinition?.code,
    payload ?? {},
    errorDefinition?.httpCode,
    errorDefinition?.grpcCode,
  );
};

export const throwError = (
  errorDefinition: ErrorThrowDefinition,
  payload: Record<string, any> = {},
  messageOverride?: string,
): void => {
  throw buildError(errorDefinition, payload, messageOverride);
};
