export interface CustomErrorLogData {
  errorClass: string;
  errorMessage: string;
  metadata: Record<string, unknown>;
}

export interface ErrorThrowDefinition {
  name: string;
  message: string;
  code: number;
  httpCode: number;
  grpcCode: number;
}

export type CustomErrorPayload = Record<string, any>;

export interface CustomHttpError {
  getHttpStatusCode(): number;
}

export interface CustomGrpcError {
  getGrpcStatusCode(): number;
}

export class CustomError
  extends Error
  implements CustomHttpError, CustomGrpcError
{
  public name: string;
  public errorCode: number;
  public message: string;
  public payload?: CustomErrorPayload | undefined;
  public securePayload?: unknown;
  public httpCode: number;
  public grpcCode: number;

  constructor(
    name: string,
    message?: string,
    errorCode?: number,
    payload?: CustomErrorPayload,
    httpCode?: number,
    grpcCode?: number,
    securePayload?: unknown,
  ) {
    super(name || 'ANONYMOUS_ERROR');
    this.name = name || 'Unknown';
    this.message = message || 'Unknown error';

    // Set error code
    this.errorCode = errorCode || 1000;

    // Set payload
    this.payload = payload;

    // Set http code
    this.httpCode = httpCode || 500;

    // Set grpc code
    this.grpcCode = grpcCode || 2;

    // Set secure payload
    this.securePayload = securePayload;

    Object.setPrototypeOf(this, CustomError.prototype);
  }

  public getLogData(): CustomErrorLogData {
    return {
      errorClass: this.name,
      errorMessage: this.message,
      metadata: this.getLogDataExtra(),
    };
  }

  protected getLogDataExtra(): CustomErrorPayload {
    return this.payload && this.payload.stack ? this.payload.stack : '';
  }

  getName(): string {
    return this.name;
  }

  getMessage(): string {
    return this.message;
  }

  getHttpStatusCode(): number {
    return this.httpCode;
  }

  getGrpcStatusCode(): number {
    return this.grpcCode;
  }

  getErrorCode(): number {
    return this.errorCode;
  }

  getPayload(): CustomErrorPayload {
    return this.payload;
  }
}
