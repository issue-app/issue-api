import { ErrorThrowDefinition } from './types';
import { GrpcCodes, HttpCodes } from './codes';

export const CustomErrors: Record<string, ErrorThrowDefinition> = {
  USER_NOT_FOUND: {
    name: 'USER_NOT_FOUND',
    message: 'User not found',
    code: 2004,
    httpCode: HttpCodes.NOT_FOUND,
    grpcCode: GrpcCodes.NOT_FOUND,
  },
  UNAUTHORIZED_ACCESS: {
    name: 'UNAUTHORIZED_ACCESS',
    message: 'You are not authorized for this!',
    code: 1001,
    httpCode: HttpCodes.UNAUTHORIZED,
    grpcCode: GrpcCodes.UNAUTHENTICATED,
  },

  USER_ALREADY_REGISTERED: {
    name: 'USER_ALREADY_REGISTERED',
    message: 'This user is already registered. Please try again.',
    code: 1005,
    httpCode: HttpCodes.NOT_ACCEPTABLE,
    grpcCode: GrpcCodes.UNAVAILABLE,
  },
  ISSUE_NOT_FOUND: {
    name: 'ISSUE_NOT_FOUND',
    message: 'Issue not found',
    code: 2004,
    httpCode: HttpCodes.NOT_FOUND,
    grpcCode: GrpcCodes.NOT_FOUND,
  },
  BAD_REQUEST: {
    name: 'BAD_REQUEST',
    message: 'The action you are initiating is not valid!',
    code: 1000,
    httpCode: HttpCodes.BAD_REQUEST,
    grpcCode: GrpcCodes.ABORTED,
  },
};
