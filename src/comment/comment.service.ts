import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from './entity/comment.entity';
import { Repository } from 'typeorm';
import { Issue } from '../issue/entity/issue.entity';
import {
  CreateCommentRequest,
  PaginatedComments,
  PaginationOptions,
} from '../issue/issue.types';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
  ) {}

  async createComment(
    issue: Issue,
    { text }: CreateCommentRequest,
  ): Promise<Comment> {
    const comment = new Comment();
    comment.text = text;
    comment.issue = issue;
    return this.commentRepository.save(comment);
  }

  async findAndCountAllComments(
    paginationOptions: PaginationOptions,
    issue: Issue,
  ): Promise<PaginatedComments> {
    const { page, limit }: PaginationOptions = paginationOptions;
    const skip: number = (page - 1) * limit;

    const [comments, total] = await this.commentRepository
      .createQueryBuilder('comment')
      .leftJoinAndSelect('comment.issue', 'issue')
      .where('comment.issue_id = :issueId', { issueId: issue.id })
      .skip(skip)
      .take(limit)
      .getManyAndCount();

    const totalItems: number = total;
    const itemsPerPage = Number(limit);
    const totalPages: number = Math.ceil(total / limit);
    const currentPage = Number(page);

    return {
      comments,
      pagination: {
        totalItems,
        itemsPerPage,
        totalPages,
        currentPage,
      },
    };
  }
}
