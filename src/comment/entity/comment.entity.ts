import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Issue } from '../../issue/entity/issue.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Comment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({ example: 'This is a comment for the issue' })
  @Column()
  text: string;

  @ManyToOne(() => Issue, (issue) => issue.comments)
  @JoinColumn({
    name: 'issue_id',
    referencedColumnName: 'id',
  })
  issue: Issue;
}
