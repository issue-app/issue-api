import { applyDecorators, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiConsumes } from '@nestjs/swagger';
import { CustomError } from '../error/types';
import { CustomErrors } from '../error/customError';

export const maxFileSize = 4 * 1024 * 1024;
export const PhotoUpload = (): ReturnType<typeof applyDecorators> =>
  applyDecorators(
    ApiConsumes('multipart/form-data'),
    UseInterceptors(
      FileInterceptor('file', {
        limits: {
          fieldNameSize: 128,
          fileSize: maxFileSize,
        },
        fileFilter: (_req, file, callback) => {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const isImage = this.isImage({
            fileName: file.originalname,
            fileType: file.mimetype,
          });

          const error = new CustomError(
            CustomErrors.BAD_REQUEST.name,
            'Only images can be uploaded (.jpg, .jpeg, .png).',
            CustomErrors.BAD_REQUEST.httpCode,
          );

          return callback(isImage ? null : error, isImage);
        },
      }),
    ),
  );

export const isImage = (payload: {
  fileName: string;
  fileType: string;
}): boolean => {
  const fileTypeDetails = payload.fileType.split('/');
  return (
    !!payload.fileName.match(/\.(jpg|jpeg|png)$/) &&
    fileTypeDetails[0] === 'image' &&
    ['jpg', 'jpeg', 'png'].includes(fileTypeDetails[1])
  );
};
