import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from '../auth/entities/user.entity';

export const ActiveUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): User => {
    return ctx.switchToHttp().getRequest().user;
  },
);
