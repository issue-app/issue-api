export const doc = `# Introduction

Issue API exposes a set of API methods ready for direct usage by mobile/web applications. Those method provide basic functionality required by front-ends to be able to build a user friendly UX/UI with following funcitonalities:

* Authentication
* Creating of the  issues 
* Management of the comments related to the specific issue
* Management  of the files related to the specific issue

# Authentication

The Issue API authenticates users by using username-password credentials and issuing an access token. Access token should be sent in headers of all subsequent calls:

\`Authoroziation: Bearer {access_token}\`

# Response structure

All responses received from the Issue API comply to a specific structure:

\`\`\`
{
  "code": number,
  "success": boolean,
  "errors": Error[]
  "data": Map<string, object>
}
\`\`\`

* **code**

    Code field represent status of the response. For successful responses the code is always 200.

* **success**

    Success boolean field returns status of the operation.

* **data**

    Data object returns actual data returned by the API.

* **errors**

    Errors key stores all errors which happened during the execution of the request. Usually empty array (\`[]\`) for successful API calls.

## Error structure

All erros are implementing following data structure:

\`\`\`
{
  "message": string,
  "code": number,
  "name": string,
  "payload": Map<string, object>|null
}
\`\`\`

* **message**

    A user-readable message.

* **code**

    Code representing an error.

* **name**

    Textual representation of the error, eg "INVALID_USERNAME_OR_PASSWORD"

* **payload**

    If required, error can contain some payload which may help with identifying the offending field or pinpointing the exact issue.

## Pagination

If API needs to return paginated data, following structure is used:

\`\`\`
{
  "code": 200,
  "success": true,
  "errors": [],
  "data": {
    "items": [
      {
        "fileName": "IMG-0796.jpg",
        "mimetype": "jpg",
        "publicUrl": "https://url.s3.eu-central-1.amazonaws.com/IMG-0796.jpg"
      }
    ],
    "pagination": {
      "totalItems": 0,
      "itemsPerPage": 0,
      "totalPages": 0,
      "currentPage": 0
    }
  }
}

`;
