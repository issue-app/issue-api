export const jwtConstants = {
  secret: process.env.JWT_SECRET || '8bfbf7161a96e687d748af1ebfb22d9d',
  accessTokenExpiresIn: process.env.JWT_ACCESS_TOKEN_DURATION || '1y',
  refreshTokenExpiresIn: process.env.JWT_REFRESH_TOKEN_DURATION || '1y',
};
