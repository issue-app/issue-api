import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constancts';
import { compare as comparePassword } from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { hash } from 'bcrypt';
import { Login, LoginRequest, RegisterUserRequest } from './auth.types';
import { CustomErrors } from '../error/customError';
import { throwError } from '../error/error';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async findByUsername(username: string): Promise<User> {
    const user = await this.userRepository.findOne({ where: { username } });

    if (!user) {
      throwError(CustomErrors.NOT_FOUND);
    }

    return user;
  }

  async login({ username, password }: LoginRequest): Promise<Login> {
    const user = await this.findByUsername(username);
    const result = await comparePassword(password, user.password);
    if (!result) {
      throw new HttpException('Wrong password', HttpStatus.FORBIDDEN);
    }
    const accessToken = this.jwtService.sign(
      {
        userId: user.id,
      },
      { expiresIn: jwtConstants.accessTokenExpiresIn },
    );
    return { accessToken };
  }

  async registerUser(registerUserRequest: RegisterUserRequest): Promise<User> {
    const { username, password, email, firstName, lastName } =
      registerUserRequest;

    const userDb: User = await this.userRepository.findOne({
      where: { username },
    });
    if (userDb) {
      throwError(CustomErrors.USER_ALREADY_REGISTERED);
    }

    const user = new User();
    user.username = username;
    user.password = await hash(password, 10);
    user.email = email;
    user.firstName = firstName;
    user.lastName = lastName;
    user.createdAt = new Date();
    user.updatedAt = new Date();

    await this.userRepository.save(user);
    return user;
  }

  async findById(id: string): Promise<User> {
    const user: User = await this.userRepository.findOne({ where: { id } });

    if (!user) {
      throwError(CustomErrors.USER_NOT_FOUND);
    }

    return user;
  }
}
