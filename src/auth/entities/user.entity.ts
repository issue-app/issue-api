import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Issue } from '../../issue/entity/issue.entity';

@Entity({ name: 'user' })
export class User {
  @ApiProperty({
    format: 'uuid',
    example: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
  })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({ example: 'john.doe@everseen.com' })
  @Column({ unique: true })
  email: string;

  @ApiProperty({ example: 'john.doe' })
  @Column()
  username: string;

  @ApiProperty({ example: 'John' })
  @Column()
  firstName: string;

  @Exclude()
  @ApiHideProperty()
  @Column()
  password: string;

  @ApiProperty({ example: 'Doe' })
  @Column()
  lastName: string;

  @CreateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
    comment: 'Create time',
  })
  public createdAt: Date;

  @OneToMany(() => Issue, (issue) => issue.user)
  issues: Issue[];

  @UpdateDateColumn({
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
    comment: 'Last update time',
  })
  public updatedAt: Date;
}
