import { Body, Controller, Post } from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { createGenericResponse } from '../responses/response';
import { AuthService } from './auth.service';
import {
  Login,
  LoginRequest,
  LoginResponse,
  RegisterUserRequest,
  RegisterUSerResponse,
} from './auth.types';
import { User } from './entities/user.entity';

@Controller('v1/auth')
@ApiTags('Authentication')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  @ApiOkResponse({
    description: 'JWT returned',
    type: LoginResponse,
  })
  @ApiOperation({
    summary: 'Login',
    description: 'Authenticates the user.',
  })
  @ApiBody({
    type: LoginRequest,
  })
  async login(@Body() loginRequest: LoginRequest): Promise<LoginResponse> {
    const response: Login = await this.authService.login(loginRequest);
    return createGenericResponse(response);
  }

  @ApiOperation({
    summary: 'Register user',
    description: 'User Registration',
  })
  @ApiCreatedResponse({
    description: 'Creates User',
    type: RegisterUSerResponse,
  })
  @Post('/registration')
  async registerUser(
    @Body() registerUserRequest: RegisterUserRequest,
  ): Promise<RegisterUSerResponse> {
    const response: User = await this.authService.registerUser(
      registerUserRequest,
    );
    return createGenericResponse(response, null, 201);
  }
}
