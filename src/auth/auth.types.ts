import { ApiProperty } from '@nestjs/swagger';
import { Column } from 'typeorm';
import { IsNotEmpty } from 'class-validator';
import { GenericResponse } from '../responses/response';
import { User } from './entities/user.entity';

export class Login {
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
  })
  accessToken: string;
}

export class LoginRequest {
  @ApiProperty({ example: 'nikola.bozic' })
  @IsNotEmpty()
  readonly username: string;
  @ApiProperty({ example: '1234' })
  @IsNotEmpty()
  readonly password: string;
}

export class RegisterUserRequest {
  @ApiProperty({ example: 'nikola.bozic.bg@gmail.com' })
  email: string;

  @ApiProperty({ example: 'nikola.bozic' })
  username: string;

  @ApiProperty({ example: 'Nikola' })
  firstName: string;

  @ApiProperty({ example: '1234' })
  password: string;

  @ApiProperty({ example: 'Bozic' })
  @Column()
  lastName: string;
}

export class LoginResponse extends GenericResponse<Login> {
  @ApiProperty({
    type: Login,
  })
  data: Login;
}

export class RegisterUSerResponse extends GenericResponse<User> {
  @ApiProperty({
    type: User,
  })
  data: User;
}
