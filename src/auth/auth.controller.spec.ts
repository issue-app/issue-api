import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { createGenericResponse } from '../responses/response.dto';
import {
  LoginRequest,
  LoginResponse,
  RegisterUserRequest,
  RegisterUSerResponse,
} from './auth.types';
import { User } from './entities/user.entity';
import { JwtService } from '@nestjs/jwt';

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        {
          provide: JwtService,
          useValue: {
            // mock the methods of JwtService as needed
            sign: jest.fn(),
          },
        },
      ],
    }).compile();

    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  // describe('login', () => {
  //   it('should return a LoginResponse object with a JWT token', async () => {
  //     const loginRequest: LoginRequest = {
  //       // provide login request data here
  //     };
  //
  //     const expectedLoginResponse: LoginResponse = {
  //       // create your expected LoginResponse object here
  //     };
  //
  //     jest.spyOn(authService, 'login').mockResolvedValue(expectedLoginResponse);
  //
  //     const result = await authController.login(loginRequest);
  //
  //     expect(result).toEqual(createGenericResponse(expectedLoginResponse));
  //   });
  // });

  describe('registerUser', () => {
    it('should return a RegisterUserResponse object with a newly created user', async () => {
      const registerUserRequest: RegisterUserRequest = {
        email: 'nikola.bozic.bg@gmail.com',
        username: 'nikola.bozic',
        firstName: 'Nikola',
        password: '1234',
        lastName: 'Bozic',
      };

      const expectedUser: User = {
        id: '1234',
        email: 'nikola.bozic.bg@gmail.com',
        username: 'nikola.bozic',
        firstName: 'Nikola',
        password: '1234',
        lastName: 'Bozic',
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      jest.spyOn(authService, 'registerUser').mockResolvedValue(expectedUser);

      const result = await authController.registerUser(registerUserRequest);

      expect(result).toEqual(createGenericResponse(expectedUser));
    });
  });
});
