import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiProperty,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

export interface Response<T> {
  readonly message: string;
  readonly data: T;
  readonly statusCode: number;
}

export class GenericResponse<T = null> {
  @ApiProperty({
    example: 200,
    type: 'number',
  })
  code = 200;

  @ApiProperty()
  success: boolean;

  @ApiProperty({
    example: [],
  })
  errors: ResponseError[] = [];

  @ApiProperty()
  data: T = null;

  constructor(data: T = null) {
    this.data = data;
  }
}

export class ResponseError {
  @ApiProperty()
  message: string;

  @ApiProperty()
  code?: number;

  @ApiProperty()
  name?: string;

  @ApiProperty()
  payload?: undefined | Record<string, unknown>;
}

export const createGenericResponse = <T>(
  data: T | null,
  errors: ResponseError[] = [],
  code = 200,
): GenericResponse<T> => ({
  code,
  data,
  success: !(errors && errors.length),
  errors,
});

export const CustomApiUnauthorizedResponse = (): MethodDecorator &
  ClassDecorator =>
  ApiUnauthorizedResponse({
    description: 'Unauthorized.',
    type: UnauthorizedResponse,
  });

export const CustomApiNotFoundResponse = (): MethodDecorator & ClassDecorator =>
  ApiNotFoundResponse({
    description: 'Not found.',
    type: NotFoundResponse,
  });

export const CustomApiApiBadRequestResponse = (): MethodDecorator &
  ClassDecorator =>
  ApiBadRequestResponse({
    description: 'Bad request.',
    type: BadRequestResponse,
  });

export class UnauthorizedResponse extends GenericResponse<null> {
  @ApiProperty({
    example: false,
  })
  success = false;

  @ApiProperty({
    example: 403,
  })
  code = 403;

  @ApiProperty({
    type: [ResponseError],
    example: [
      {
        message: 'UNAUTHORIZED',
        code: 403,
      },
    ],
  })
  errors: ResponseError[] = [
    {
      message: 'UNAUTHORIZED',
      code: 403,
    },
  ];

  @ApiProperty()
  data: null;
}

export class BadRequestResponse extends GenericResponse<null> {
  @ApiProperty({
    example: false,
  })
  success = false;

  @ApiProperty({
    type: [ResponseError],
    example: [
      {
        message: 'An error ocurred',
        code: 1000,
        name: 'ERROR_NAME',
        payload: {
          key: 'value',
        },
      },
    ],
  })
  errors: ResponseError[] = [
    {
      message: 'FIELD_ERROR_MESSAGE',
      code: 400,
    },
  ];

  @ApiProperty()
  data: null;
}

export class NotFoundResponse extends GenericResponse<null> {
  @ApiProperty({
    example: false,
  })
  success = false;

  @ApiProperty({
    type: [ResponseError],
    example: [
      {
        message: 'NOT_FOUND',
        code: 404,
      },
    ],
  })
  errors: ResponseError[] = [
    {
      message: 'NOT_FOUND',
      code: 404,
    },
  ];

  @ApiProperty()
  data: null;
}
