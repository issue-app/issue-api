import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from '../auth/auth.service';
import { throwError } from '../error/error';
import { CustomError } from '../error/types';
import { CustomErrors } from '../error/customError';

@Injectable()
export class SessionMiddleware implements NestMiddleware {
  constructor(
    private readonly jwtService: JwtService,
    private readonly authService: AuthService,
  ) {}

  async use(req: Request, res: Response, next: () => void): Promise<void> {
    try {
      const headers = req.header('Authorization');

      if (!headers) {
        throwError(CustomErrors.UNAUTHORIZED_ACCESS);
      }
      const token = headers.replace('Bearer ', '');

      this.jwtService.verify(token);
      const decodedToken = this.jwtService.decode(token);

      req['user'] = await this.authService.findById(decodedToken['userId']);
    } catch (error) {
      throwError(CustomErrors.UNAUTHORIZED_ACCESS);
    }
    next();
  }
}
