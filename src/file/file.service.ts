import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { File } from './entity/file.entity';
import { Repository } from 'typeorm';
import { Issue } from '../issue/entity/issue.entity';
import * as AWS from 'aws-sdk';
import { PaginatedFiles, PaginationOptions } from '../issue/issue.types';
import { throwError } from '../error/error';
import { CustomErrors } from '../error/customError';

@Injectable()
export class FileService {
  private s3: AWS.S3;
  constructor(
    @InjectRepository(File)
    private readonly fileRepository: Repository<File>,
  ) {
    this.s3 = new AWS.S3({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      region: process.env.AWS_REGION,
    });
  }

  async uploadFile(
    originalname: string,
    mimetype: string,
    buffer: Buffer,
    issue: Issue,
  ): Promise<File> {
    console.log(originalname);
    console.log(mimetype);
    console.log(buffer);

    const params = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: originalname,
      Body: buffer,
      ContentType: mimetype,
    };

    const res = await this.s3.upload(params).promise();

    const file: File = new File();
    file.fileName = originalname;
    file.mimetype = mimetype;
    file.publicUrl = res.Location;
    file.issue = issue;
    return this.fileRepository.save(file);
  }

  async findAndCountAllFiles(
    paginationOptions: PaginationOptions,
    issue: Issue,
  ): Promise<PaginatedFiles> {
    const { page, limit }: PaginationOptions = paginationOptions;
    const skip: number = (page - 1) * limit;

    const [files, total] = await this.fileRepository
      .createQueryBuilder('file')
      .leftJoinAndSelect('file.issue', 'issue')
      .where('file.issue_id = :issueId', { issueId: issue.id })
      .skip(skip)
      .take(limit)
      .getManyAndCount();

    const totalItems: number = total;
    const itemsPerPage = Number(limit);
    const totalPages: number = Math.ceil(total / limit);
    const currentPage = Number(page);

    return {
      files,
      pagination: {
        totalItems,
        itemsPerPage,
        totalPages,
        currentPage,
      },
    };
  }

  async getFileByIdAndIssue(
    issueId: string,
    fileId: string,
  ): Promise<File | undefined> {
    const file = await this.fileRepository.findOne({
      where: { id: fileId, issue: { id: issueId } },
    });
    if (!file) {
      throwError(CustomErrors.ISSUE_NOT_FOUND);
    }
    return file;
  }
}
