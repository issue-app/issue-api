import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Issue } from '../../issue/entity/issue.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({ example: 'IMG-0796.jpg' })
  @Column()
  fileName: string;

  @ApiProperty({ example: 'jpg' })
  @Column()
  mimetype: string;

  @ApiProperty({
    example: 'https://url.s3.eu-central-1.amazonaws.com/IMG-0796.jpg',
  })
  @Column()
  publicUrl: string;

  @ManyToOne(() => Issue, (issue) => issue.files)
  @JoinColumn({
    name: 'issue_id',
    referencedColumnName: 'id',
  })
  issue: Issue;
}
