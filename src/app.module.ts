import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { IssueModule } from './issue/issue.module';
import { CommentModule } from './comment/comment.module';
import { FileModule } from './file/file.module';

@Module({
  imports: [
    NestConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [
        '../../.env.dev.global.local',
        '../../.env.dev.global',
        '.env.local',
        '.env',
        '.env.dev.local',
        '.env.dev',
      ],
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (): TypeOrmModuleOptions => ({
        type: process.env.DB_TYPE as any,
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        entities: ['dist/**/*.entity{.ts,.js}'],
        migrations: ['dist/**/database/migration/**/*{.ts,.js}'],
        subscribers: ['dist/**/*.subscriber/{.ts,.js}'],
        migrationsRun: true,
        synchronize: true,
        uuidExtension: 'pgcrypto',
        schema: 'public',
        logging: process.env.DB_LOGGING === 'true',
        migrationsTableName: 'florence_migration',
        namingStrategy: new SnakeNamingStrategy(),
        ssl: false,
        options: { encrypt: false },
      }),
    }),
    AuthModule,
    IssueModule,
    CommentModule,
    FileModule,
  ],
})
export class AppModule {}
