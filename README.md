# Issue API

---

## Prerequisites

- Recomendation is to useyan instead of npm. Please install [Yarn](https://classic.yarnpkg.com/en/docs/install/).
- Install latest NodeJS LTS version (currently v16)
- Install ``TypeScript`` globally with `yarn global add typescript`
- Install ``ts-node`` globally with `yarn global add ts-node`
- Install [Docker](https://www.docker.com/products/docker-desktop) on your machine.
    - On Linux add your user to `docker` group with `sudo usermod -aG docker your-user`
    - If you do not you must use `sudo` for Docker commands
- Install [docker-compose](https://docs.docker.com/compose/)
---
## Setup
- You can usee `docker-compose -f docker-compose.yml up -d` to run application and postgres service in docker
- if you run app without docker setup the project with `yarn install`
- Then start postgres service with `docker-compose -f services.yml up -d`. 
    - **Note: use `sudo` on Linux if your user is not in the docker group**
- Start project with `yarn start`
- All logs will be streamed in console. The API gateway will start on port 10000, and you can see the Swagger definitions [HERE](http://localhost:300/api)
- Note that the initial configuration of the project (by using .env.dev files) is configured to work with the `docker-compose.services.yml` file (ports and hosts are configured that way).

---

